package fr.univlille.iutinfo.r304.thermo.part1.view;


import org.junit.platform.console.shadow.picocli.CommandLine.TypeConversionException;

import fr.univlille.iutinfo.r304.thermo.part1.model.Thermogeekostat;
import fr.univlille.iutinfo.r304.utils.Observer;
import fr.univlille.iutinfo.r304.utils.Subject;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class TextView extends Stage implements ITemperatureView,Observer {
	private final Thermogeekostat model;
	Label label = new Label();

	public TextView(Thermogeekostat model) {
		this.model = model;
		model.attach(this);
		this.setScene(new Scene(getPane()));
		this.show();
	}

	@Override
	public double getDisplayedValue() {
		return this.model.getTemperature();
	}

	@Override
	public void incrementAction() {
		this.model.incrementTemperature();
	}

	@Override
	public void decrementAction() {
		this.model.decrementTemperature();
	}

	@Override
	public void update(Subject subj) {
		this.label.setText(this.getDisplayedValue()+ "");
	}

	@Override
	public void update(Subject subj, Object data) {
		this.label.setText( data +"");
	}

	public Pane getPane() {
		BorderPane root = new BorderPane();
		this.label.setText("" + this.getDisplayedValue());
		root.setCenter(this.label);
		
		Button decrement = new Button(" - ");
		decrement.setOnMouseClicked(e -> this.decrementAction());
		root.setLeft(decrement);

		Button increment = new Button(" + ");
		increment.setOnMouseClicked(e -> this.incrementAction());
		root.setRight(increment);

		HBox inputzone = new HBox();
		TextField input = new TextField();
		Button valid = new Button();
		valid.setText(">");
		valid.setOnAction(e -> {
			try {
				this.model.setTemperature(Double.parseDouble(input.getText()));
			} catch(TypeConversionException error) {
				System.out.println(error.getMessage());
			}
		});
		inputzone.getChildren().addAll(input, valid);
		root.setBottom(inputzone);

		return root;
	}
}
