package fr.univlille.iutinfo.r304.thermo.part2.model;

public enum Echelle {
	CELSIUS('C'), FAHRENHEIT('F'), KELVIN('K'), RANKINE('R'), NEWTON('N');
	private final char C;

	private Echelle(char type){
        this.C = type;
    }

	public String getName() {
		return "test";
	}

	public String getAbbrev() {
		return null;
	}

	public double fromKelvin(double d) {
		return d / (1/273.16);
	}

	public double toKelvin(double d) {
		return d * (1/273.16);
	}

}
